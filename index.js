require('dotenv').config();
const mongoose = require('mongoose');
const express = require('express');
const morgan = require('morgan');
const PORT = process.env.PORT || 8080;
const db = process.env.DATABASE;
const userRoutes = require('./routes/userRoutes');
const authRoutes = require('./routes/authRoutes');
const truckRoutes = require('./routes/truckRoutes');
const loadRoutes = require('./routes/loadRoutes');


// const noteRoutes = require('./routes/noteRoutes');

const app = express();
app.use(express.json());
const logger = morgan('dev');
app.use(logger);

mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true }).then(() => console.log('database successfully connected')).catch((err) => console.log(err));

// app.listen(PORT, () => {
//     console.log(`Server has been started on ${PORT}...`)
// });
app.use(authRoutes);
app.use(userRoutes);
app.use(truckRoutes);
app.use(loadRoutes);


app.listen(PORT, (err) => {
    if (err) {
        return console.log('something happened', err)
    }
    console.log(`server is listening on ${PORT}`);
});