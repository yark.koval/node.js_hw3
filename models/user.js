const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({
    role: {
        type: String,
        required: true,
        enum: ['DRIVER', 'SHIPPER'],
    },
    email: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    created_date: {
        type: Date,
        default: new Date().toISOString()
    },
}, { versionKey: false });

// module.exports = mongoose.model('User', userSchema);


const User = mongoose.model('User', userSchema);

module.exports = User;