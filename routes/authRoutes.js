const express = require('express');
const { loginUser, registerUser, recoverPassword } = require('../controllers/authСontroller');
const router = express.Router();

router.post('/api/auth/login', loginUser);
router.post('/api/auth/register', registerUser);
router.post('/api/auth/forgot_password', recoverPassword);
module.exports = router;