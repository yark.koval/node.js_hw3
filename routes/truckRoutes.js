const express = require('express');
const router = express.Router();
const { addTruck, assignTruck, getTrucks, getTruck, goToNextLoadState } = require('../controllers/truckController');
const truckMiddleware = require('../middlewares/truckMiddleware');
const authMiddleware = require('../middlewares/authMiddleware');

router.post('/api/trucks', [authMiddleware, truckMiddleware], addTruck);
router.get('/api/trucks', [authMiddleware, truckMiddleware], getTrucks);
router.get('/api/trucks/:id', [authMiddleware, truckMiddleware], getTruck);
router.post('/api/trucks/:id/assign', [authMiddleware, truckMiddleware], assignTruck);
// router.delete('/api/trucks/:id', [authMiddleware, truckMiddleware], deleteTruck);
// router.put('/api/trucks/:id', [authMiddleware, truckMiddleware], updateTruck);
// router.get('/api/loads/active', [authMiddleware, truckMiddleware], getActiveLoad);
router.patch('/api/loads/active/state', [authMiddleware, truckMiddleware], goToNextLoadState);

module.exports = router;