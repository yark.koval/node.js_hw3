const User = require('../models/user');

module.exports = async function(req, res, next) {
    if (req.method === 'OPTIONS') {
        next();
    }

    try {
        const user = await User.findOne({ email: req.user.email });
        if (user.role === 'DRIVER') {
            next();
        } else {
            return res.status(400).send({ message: 'this only for drivers' });
        }
    } catch (error) {
        console.log(error);
        return res.status(400).send({ message: 'some client error' });
    }
};