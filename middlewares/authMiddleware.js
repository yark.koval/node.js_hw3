const jwt = require('jsonwebtoken');
require('dotenv').config();
const SECRET= process.env.SECRET;

module.exports = function(req, res, next) {
    try {
        const token = req.headers.authorization.split(' ')[1];
        if (!token) {
            return res.status(400).send({message: 'Some client error'});
        }

        const verified = jwt.verify(token, SECRET);
        req.user = verified;
        next();
    } catch (error) {
        console.log(error);
        return res.status(400).send({message: 'Client authorization error'});
    }
};
