const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const { userValidation } = require('../validations/userValidation');
require('dotenv').config();
const { ClientErrorHandler, ServerErrorHandler } = require('../controllers/errorController');
const SECRET = process.env.SECRET;

//
// const ClientErrorHandler = (res, error) => {
//     console.log(error);
//     res.status(400).send({message: 'Some client error'});
// };
//
// const ServerErrorHandler = (res, error) => {
//     console.log(error);
//     res.status(500).send({message: 'Some server error'});
// };

const generateAccessToken = (id, email) => {
    const payload = { id, email };
    return jwt.sign(payload, SECRET, { expiresIn: '7d' });
};

const generatePass = () => {
    let pass = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    const passLength = (Math.random() * 15) + 5;

    for (let i = 0; i < passLength; i++) {
        pass += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return pass;
};



const registerUser = (req, res) => {
    // const {username, password} = req.body;
    try {
        const { email, password, role } = req.body;
        const hashPassword = bcrypt.hashSync(password, 5);
        const user = new User({ email, password: hashPassword, role });
        user.save().then(() => res.status(200).send({ message: 'Profile created successfully' }))
            .catch((error) => ClientErrorHandler(res, error));
    } catch (error) {
        ServerErrorHandler(res, error);
    }
};

const loginUser = async(req, res) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ email });
        if (!user) {
            return res.status(400).send({ message: 'some user error' });
        }
        const validPassword = bcrypt.compare(password, user.password);
        if (!validPassword) {
            return res.status(400).send({ message: 'invalid user password' });
        }
        const token = generateAccessToken(user._id, user.email);
        return res.status(200).send({ message: 'Success', jwt_token: token });
    } catch (error) {
        ServerErrorHandler(res, error);
    }
};

const recoverPassword = async(req, res) => {
    try {
        const user = await User.findOne({ email: req.body.email });
        if (!user) {
            return res.status(400).send({ message: 'No user with such email' });
        }

        const testAccount = await nodemailer.createTestAccount();

        const transporter = nodemailer.createTransport({
            host: 'smtp.ethereal.email',
            port: 587,
            secure: false,
            auth: {
                user: testAccount.user,
                pass: testAccount.pass,
            },
        });

        const newPassword = generatePass();

        const mailOptions = {
            from: testAccount.user,
            to: req.body.email,
            subject: 'Recovering your password',
            text: `Hello, your new password is ${newPassword}`,
        };

        const hashPassword = bcrypt.hashSync(newPassword, 7);
        await User.findByIdAndUpdate(user._id, { password: hashPassword })
            .then(() => console.log('Password changed'))
            .catch((error) => ServerErrorHandler(res, error));

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
                ServerErrorHandler(res, error);
            } else {
                console.log(nodemailer.getTestMessageUrl(info));
                return res.status(200).send({ message: 'New password sent to your email address' });
            }
        });
    } catch (error) {
        ServerErrorHandler(res, error);
    }
};


module.exports = { registerUser, loginUser, recoverPassword };