const ClientErrorHandler = (res, error) => {
    console.log(error);
    res.status(400).send({ message: 'Some user error here' });
};

const ServerErrorHandler = (res, error) => {
    console.log(error);
    res.status(500).send({ message: 'Some server error' });
};

module.exports = {
    ClientErrorHandler,
    ServerErrorHandler
};