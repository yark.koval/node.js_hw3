const User = require('../models/user');
const bcrypt = require('bcryptjs');
const { ServerErrorHandler, ClientErrorHandler } = require('../controllers/errorController');


const getUserInfo = async(req, res) => {
    try {
        const user = await User.findOne({ email: req.user.email });
        const { _id, email, created_date, role } = user;
        return res.status(200).send({
            user: { _id, role, email, created_date }
        });
    } catch (error) { ServerErrorHandler(res, error); }
};

const deleteUser = async(req, res) => {
    try {
        User.findByIdAndDelete(req.user.id)
            .then(() => res.status(200).send({ message: 'Profile deleted successfully' }))
            .catch((error) => ClientErrorHandler(res, error));
    } catch (error) {
        console.log(error);
        ServerErrorHandler(res, error);
    }
};

const changePassword = async(req, res) => {
    try {
        const user = await User.findOne({ _id: req.user.id });
        const passList = req.body;
        const validCurrentPassword = bcrypt.compare(passList.oldPassword, user.password);
        if (!validCurrentPassword) {
            return res.status(400).send({ message: 'Did you forgot your current password?' });
        }
        const hashedPassword = bcrypt.hashSync(req.body.newPassword, 5);
        await User.findByIdAndUpdate(req.user.id, { password: hashedPassword }).then(() => {
                user.save();
                res.status(200).send({ message: 'Password changed successfully' });
            })
            .catch(() => res.status(400).send({ message: 'Some user error' }));
    } catch (error) {
        ServerErrorHandler(res, error);
    }
};
module.exports = { getUserInfo, deleteUser, changePassword };